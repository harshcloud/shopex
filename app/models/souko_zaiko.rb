require 'rake'
class SoukoZaiko < ApplicationRecord
	before_save :set_default_values
	after_save :set_is_disable
	belongs_to :order, class_name: 'Order', optional: true

	def set_default_values
	    usr_id = 1
	    self.created_user_id = usr_id if self.new_record?
	    self.updated_user_id = usr_id if self.changed?
	end

    def self.zaiko_stock(sku_code, quantity)
        total_stock = 0
        sold_stock = 0
        stock = SoukoZaiko.where(sku_code: sku_code, stock_type: "01")
        stock.each do |key, value|
            total_stock += key[:stock]
        end

        sold = SoukoZaiko.where(sku_code: sku_code, stock_type: "02")
        if sold.present?
            sold.each do |key, value|
                sold_stock += key[:stock]
            end
        end

        available_stock = total_stock - sold_stock
        if quantity <= available_stock
            return "available"
        elsif quantity > available_stock
            return "not available"
        end
    end

	def self.new_souko_zaiko(warehouse_code, sku_code, stock_type, stock)
		self.create!(warehouse_code: warehouse_code, sku_code: sku_code, stock_type: stock_type, stock: stock)
	end

	def set_is_disable
		total_stock = 0
		sold_stock = 0
		stock = SoukoZaiko.where(sku_code: self.sku_code, stock_type: "01")
        stock.each do |key, value|
            total_stock += key[:stock]
        end

        sold = SoukoZaiko.where(sku_code: self.sku_code, stock_type: "02")
        if sold.present?
            sold.each do |key, value|
                sold_stock += key[:stock]
            end
        end

        if sold_stock > 0
        	if total_stock == sold_stock
        		stock = SoukoZaiko.where(sku_code: self.sku_code, stock_type: "01", warehouse_code: "WEST")
        		stock.update_all(is_disable: 1)
        	else
        		self.is_disable = 0
        	end
        end
	end


    def self.check_zaiko(warehouse_code, sku_code, quantity, sum1)
      if warehouse_code == 'EAST'
        warehouse_code1 = 'WEST'
      elsif warehouse_code == 'WEST'
        warehouse_code1 = 'EAST'
      end
      code02 = SoukoZaiko.find_by_warehouse_code_and_sku_code_and_stock_type(warehouse_code1, sku_code, "01")
      sum2 = 0
      code03 = SoukoZaiko.where(warehouse_code: warehouse_code1, sku_code: sku_code, stock_type: "02")
      if code03.present?
        code03.each do |key, value|
          sum2 += key[:stock]
        end
      end
      sum3 = code02.stock - sum2
      if code02.present?
        if code02.stock >= quantity
          if sum1 == 0 && sum3 >= quantity
            SoukoZaiko.new_souko_zaiko(warehouse_code1, sku_code, "02", quantity)
          elsif sum3 < quantity && sum1 == 0
            # redirect_to order_path, flash: {error: "Stock not available."}
          elsif sum1 !=0 && sum3 >= quantity
            SoukoZaiko.new_souko_zaiko(warehouse_code, sku_code, "02", sum1)
            sum2 = quantity - sum1
            SoukoZaiko.new_souko_zaiko(warehouse_code1, sku_code, "02", sum2)
          end
        else
          available_stock = code02.stock + sum1
          if available_stock < quantity
            # redirect_to order_path, flash: {error: "Stock not available."}
          elsif available_stock == quantity
            SoukoZaiko.new_souko_zaiko(warehouse_code, sku_code, "02", sum1)
            SoukoZaiko.new_souko_zaiko(warehouse_code1, sku_code, "02", sum3)
          end
          # redirect_to order_path, flash: {error: 'Stock not available for this location please select another location'}
        end
      end
    end

end