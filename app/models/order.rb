class Order < ApplicationRecord
	before_save :set_default_values, :set_order_number
	# attr_accessor :items_attributes
	has_many :order_items, :foreign_key => "order_id", :dependent => :delete_all
	accepts_nested_attributes_for :order_items, :allow_destroy => true
	validates_presence_of :delivery_zipcode, :delivery_address

	def set_default_values
	    usr_id = 1
	    self.created_user_id = usr_id if self.new_record?
	    self.updated_user_id = usr_id if self.changed?
	end

	def set_order_number
	    next_id = Order.maximum('id')
	    self.order_number = (Time.now.strftime('%Y%m%d') + ("%.5i" %next_id)).to_s
	end

    def self.percent(num, percentage)
      	tax = (num * percentage) / 100
    end

    def self.order_items(sku_code, quantity, price, amt)
    	self.order_items.create!(sku_code: sku_code, quantity: quantity, price: price, total_amount: amt)
    end

    def self.create_your_order(pincode, state, city, area, address, tax, total_amount)
    	self.create!(delivery_zipcode: pincode, delivery_state: state, delivery_city: city, delivery_area: area, delivery_address: address, tax: tax, total_amount: total_amount)
    end

    def self.save_quantity(state_name, sku_code, quantity, total_quantity)
      begin
        available_stock = ""
        state = PrefectureCode.find_by_name(state_name)
        if state.ew_flag == '東'
          warehouse_code = 'EAST'
        else
          warehouse_code = 'WEST'
        end

        code = SoukoZaiko.find_by_warehouse_code_and_sku_code_and_stock_type(warehouse_code, sku_code, "01")
        code1 = SoukoZaiko.where(sku_code: sku_code, stock_type: "01")
        total_code1 = code1[0][:stock] + code1[1][:stock]
        sum = 0
        code2 = SoukoZaiko.where(warehouse_code: warehouse_code, sku_code: sku_code, stock_type: "02")
        if code2.present?
          code2.each do |key, value|
            sum += key[:stock]
          end
        end
        new_sum = total_code1 - sum

        if code.present?
          if quantity <= code.stock
            if sum > 0
              sum1 = code.stock - sum
              if sum1 >= quantity
                SoukoZaiko.new_souko_zaiko(warehouse_code, sku_code, "02", quantity)
              elsif sum1 < quantity
                SoukoZaiko.check_zaiko(warehouse_code, sku_code, quantity, sum1)
              end
            else
              SoukoZaiko.new_souko_zaiko(warehouse_code, sku_code, "02", quantity)
            end
          else
            if quantity > total_code1
              # redirect_to order_path, flash: {error: "Stock not available"}
            elsif quantity <= total_code1
              if sum > 0
              sum1 = code.stock - sum
                if sum1 >= quantity
                  SoukoZaiko.new_souko_zaiko(warehouse_code, sku_code, "02", quantity)
                elsif sum1 < quantity
                  SoukoZaiko.check_zaiko(warehouse_code, sku_code, quantity, sum1)
                end
              elsif (sum == 0 && quantity <= total_code1)
                new_stock = quantity - code.stock
                if warehouse_code == 'EAST'
                  warehouse_code1 = 'WEST'
                elsif warehouse_code == 'WEST'
                  warehouse_code1 = 'EAST'
                end
                SoukoZaiko.new_souko_zaiko(warehouse_code, sku_code, "02", code.stock)
                SoukoZaiko.new_souko_zaiko(warehouse_code1, sku_code, "02", new_stock)
              else
                SoukoZaiko.new_souko_zaiko(warehouse_code, sku_code, "02", quantity)
              end
            end
          end
        else
          # redirect_to order_path, flash: {error: "stock not available."}
        end
      rescue => e
        raise ActiveRecord::Rollback
      end
    end
end