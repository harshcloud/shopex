require 'rake'
class PrefectureCode < ApplicationRecord
	before_save :set_default_values

	def set_default_values
	    usr_id = 1
	    self.created_user_id = usr_id if self.new_record?
	    self.updated_user_id = usr_id if self.changed?
	end

end