class OrderItemsController < ApplicationController
	skip_before_action :verify_authenticity_token
	# protect_from_forgery prepend: true, with: :exception
	protect_from_forgery with: :null_session
	before_action :authenticate_user!, :set_order, only: [:edit, :update]
	# before_action :set_order, only: [:edit, :update]

	# def new
	# 	@order = Order.new
	# 	@order_items = @order.order_items
	# end

	# private

    # Use callbacks to share common setup or constraints between actions.
  	# def set_order
    	# order = Order.find_by_id(params[:id])
  	# end

end