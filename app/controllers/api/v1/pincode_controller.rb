module Api
  	module V1
    	class PincodeController < ActionController::Base

    		def test
		        render json: { test: 'test' }, status: :ok
		    end

    		def get_pincode_details
		  		address = ZipCodeJp.find(params[:pincode])
		  		if address.present?
		  			render json: {state: address.prefecture , city: address.city, area: address.town}
		  		else
		  			render json: {state: '' , city: '', area: ''}
		  		end
		  	end

    	end
	end
end

