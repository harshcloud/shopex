class OrderController < ApplicationController
  skip_before_action :verify_authenticity_token
  # protect_from_forgery prepend: true, with: :exception
  protect_from_forgery with: :null_session
  before_action :authenticate_user!


    def index
      sku_code = SoukoZaiko.select(:sku_code).all
    end

    def create_order
      begin
        stock = []
        if params[:r].present?
          params[:r].each do |key, value|
            quantity = value[:quantity].gsub(',', '').to_i
            stock1 = SoukoZaiko.zaiko_stock(value[:sku_code], quantity)
            stock.push(stock1)
          end
        end

        av_stock = stock.uniq.join(', ')
        if av_stock == "available"
          amt = params[:r][:quantity].to_i * params[:r][:price].to_i
          tax = Order.percent(amt, 10)
          total_amount = amt + tax
          order = Order.create_your_order(params[:pincode], params[:state], params[:city], params[:area], params[:address], tax, total_amount)
          total = 0
          total_quantity = 0
          if params[:r].present?
            params[:r].each do |key, value|
              quantity = value[:quantity].gsub(',', '').to_i
              price = value[:price].gsub(',', '').to_i
              amt1 = quantity * price
              order.order_items.create!(sku_code: value[:sku_code], quantity: quantity, price: price, total_amount: amt1)
              total += amt1
              total_quantity += value[:quantity].gsub(',', '').to_i
              Order.save_quantity(params[:state],value[:sku_code],value[:quantity].gsub(',', '').to_i, total_quantity)
            end
          end
          total1 = total + amt
          new_order = Order.find_by_id(order.id)
          if new_order.nil?
          else
            new_order.update!(tax: Order.percent(total1, 10), total_amount: (total1 + Order.percent(total1, 10)))
            redirect_to order_path, flash: {success: 'Your order is successfully created'}
          end
        else
          redirect_to order_path, flash: {error: "Stock not available"}
        end
      rescue => e
        raise ActiveRecord::Rollback
      end
    end
end