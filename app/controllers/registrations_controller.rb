class RegistrationsController < Devise::RegistrationsController
  # prepend_before_action :set_minimum_password_length, only: [:new, :edit]
  # respond_to :html, :js

    def create
      begin
        user = User.find_or_initialize_by(email: params[:user][:email])
        if user.persisted?
          redirect_to new_user_registration_path, flash: {error: 'User already exist please use another email'}
        else
          user.email = params[:user][:email]
          user.password = params[:user][:password]
          user.password_confirmation = params[:user][:password_confirmation]
          if user.password.length >= 6
            if user.password == user.password_confirmation
              user.save
              sign_in(user)
              redirect_to welcome_path, flash: {success: 'Successfully Signed In.'}
            else
              redirect_to new_user_registration_path, flash: {error: 'confirm password does not match' }
            end
          else
            redirect_to new_user_registration_path, flash: {error: 'Please use password with minimum six characters' }
          end
        end
      rescue => e
        raise ActiveRecord::Rollback
      end
    end
end