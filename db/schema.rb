# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20201222091244) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "order_items", force: :cascade do |t|
    t.string "sku_code", limit: 50, null: false
    t.bigint "quantity", null: false
    t.integer "price", null: false
    t.integer "total_amount", null: false
    t.bigint "order_id"
    t.bigint "created_user_id"
    t.bigint "updated_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_user_id"], name: "index_order_items_on_created_user_id"
    t.index ["order_id"], name: "index_order_items_on_order_id"
    t.index ["updated_user_id"], name: "index_order_items_on_updated_user_id"
  end

  create_table "orders", force: :cascade do |t|
    t.string "order_number", limit: 20, null: false
    t.integer "total_amount", null: false
    t.string "delivery_zipcode", limit: 10, null: false
    t.string "delivery_state", limit: 50, null: false
    t.string "delivery_city", limit: 50, null: false
    t.string "delivery_area", limit: 50, null: false
    t.string "delivery_address", limit: 200, null: false
    t.integer "tax", null: false
    t.bigint "created_user_id"
    t.bigint "updated_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_user_id"], name: "index_orders_on_created_user_id"
    t.index ["updated_user_id"], name: "index_orders_on_updated_user_id"
  end

  create_table "prefecture_codes", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.string "code", limit: 50, null: false
    t.string "ew_flag", limit: 50, null: false
    t.bigint "created_user_id"
    t.bigint "updated_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_user_id"], name: "index_prefecture_codes_on_created_user_id"
    t.index ["updated_user_id"], name: "index_prefecture_codes_on_updated_user_id"
  end

  create_table "souko_zaikos", force: :cascade do |t|
    t.string "warehouse_code", limit: 10, null: false
    t.string "sku_code", limit: 10, null: false
    t.string "stock_type", limit: 10, null: false
    t.bigint "stock", null: false
    t.bigint "created_user_id"
    t.bigint "updated_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "is_disable", limit: 2, default: 0
    t.index ["created_user_id"], name: "index_souko_zaikos_on_created_user_id"
    t.index ["updated_user_id"], name: "index_souko_zaikos_on_updated_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "order_items", "orders"
  add_foreign_key "order_items", "users", column: "created_user_id"
  add_foreign_key "order_items", "users", column: "updated_user_id"
  add_foreign_key "orders", "users", column: "created_user_id"
  add_foreign_key "orders", "users", column: "updated_user_id"
  add_foreign_key "prefecture_codes", "users", column: "created_user_id"
  add_foreign_key "prefecture_codes", "users", column: "updated_user_id"
  add_foreign_key "souko_zaikos", "users", column: "created_user_id"
  add_foreign_key "souko_zaikos", "users", column: "updated_user_id"
end
