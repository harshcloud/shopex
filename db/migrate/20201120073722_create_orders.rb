class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.string :order_format, limit: 20, null: false
      t.integer :total_amount, null: false
      t.string :delivery_zipcode, limit: 10, null: false
      t.string :delivery_state, limit: 50, null: false
      t.string :delivery_city, limit: 50, null: false
      t.string :delivery_area, limit: 50, null: false
      t.string :delivery_address, limit: 200, null: false
      t.integer :tax, null: false
      t.references :created_user, index: true, foreign_key: {to_table: :users}
	  t.references :updated_user, index: true, foreign_key: {to_table: :users}
      t.timestamps
    end
  end
end
