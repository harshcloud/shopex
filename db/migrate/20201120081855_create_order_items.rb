class CreateOrderItems < ActiveRecord::Migration[5.1]
  def change
    create_table :order_items do |t|
    	t.string :sku_code, limit: 50, null: false
    	t.integer :quantity, limit: 5, null: false
    	t.integer :price, null: false
    	t.integer :total_amount, null: false
    	t.references :order, index: true, foreign_key: {to_table: :orders}
    	t.references :created_user, index: true, foreign_key: {to_table: :users}
		t.references :updated_user, index: true, foreign_key: {to_table: :users}
	    t.timestamps
    end
  end
end
