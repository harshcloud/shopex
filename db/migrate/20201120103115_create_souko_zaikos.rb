class CreateSoukoZaikos < ActiveRecord::Migration[5.1]
  def change
    create_table :souko_zaikos do |t|
    	t.string :warehouse_code, limit: 10, null: false
    	t.string :sku_code, limit: 10, null: false
    	t.string :stock_type, limit: 10, null: false
    	t.integer :stock, limit: 5, null: false
    	t.references :created_user, index: true, foreign_key: {to_table: :users}
		t.references :updated_user, index: true, foreign_key: {to_table: :users}
	    t.timestamps
    end
  end
end
