class AddColumnInSoukoZaiko < ActiveRecord::Migration[5.1]
  def change
  	add_column :souko_zaikos, :is_disable, :integer, limit: 2, default: 0
  end
end
