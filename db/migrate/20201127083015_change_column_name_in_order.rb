class ChangeColumnNameInOrder < ActiveRecord::Migration[5.1]
  def change
  	rename_column :orders, :order_format, :order_number
  end
end
