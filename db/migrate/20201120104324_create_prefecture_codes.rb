class CreatePrefectureCodes < ActiveRecord::Migration[5.1]
  def change
    create_table :prefecture_codes do |t|
    	t.string :name, limit: 50, null: false
    	t.string :code, limit: 50, null: false
    	t.string :ew_flag, limit: 50, null: false
    	t.references :created_user, index: true, foreign_key: {to_table: :users}
		t.references :updated_user, index: true, foreign_key: {to_table: :users}
	    t.timestamps
    end
  end
end
