Rails.application.routes.default_url_options[:host] = "localhost"
Rails.application.routes.draw do
  # devise_for :users
  devise_for :users, :controllers => {:registrations => "registrations"}
  get 'home/index'
  root 'home#index'
  get 'order' => 'order#index', as: 'order'
  get 'welcome' => 'home#welcome', as: 'welcome'
  get 'enquiry' => 'home#contact', as: 'enquiry'
  get 'about' => 'home#about', as: 'about'
  get 'faq' => 'home#faq', as: 'faq'
  post 'create_order' => 'order#create_order', as: 'create_order'
  # get '/get_pincode_details', to: 'order#get_pincode_details', as: :get_pincode_details
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api do
    namespace :v1 do
      get '/test', to: 'pincode#test', as: :test
      get '/get_pincode_details', to: 'pincode#get_pincode_details', as: :get_pincode_details
    end
  end
end
